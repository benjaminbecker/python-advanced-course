class GuessingGame:
    def __init__(self, target: int):
        self.__target = target

    def guess(self, number: int):
        if number < self.__target:
            raise ToLowException
        if number > self.__target:
            raise ToHighException
        return True


class ToLowException(Exception):
    pass


class ToHighException(Exception):
    pass
