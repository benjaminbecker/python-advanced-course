import lib
import random

target = random.randint(0, 100)
guessing_game = lib.GuessingGame(target)

finished = False
while not finished:
    user_input = input("guess the number: ")
    guess = int(user_input)

    try:
        finished = guessing_game.guess(guess)
    except lib.ToHighException:
        print(f"{guess} is to high.")
    except lib.ToLowException:
        print(f"{guess} is to low.")
    else:
        print("Congratulations!!")
