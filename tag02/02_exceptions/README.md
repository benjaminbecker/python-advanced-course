# Exceptions

[Python Dokumentation](https://docs.python.org/3/tutorial/errors.html)

[Built-in Exceptions](https://docs.python.org/3/library/exceptions.html)

# Aufgabe

Erweitern Sie die Werkzeugreservierung mit einer selbst definierten Exception, für den Fall, dass ein Werkzeug schon gebucht ist.
