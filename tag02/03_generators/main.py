# einfache Funktion mit yield

# mehrere yields
def multiple_yields():
    print("before first")
    yield 1
    print("before second")
    yield 2
    print("after second")


gen = multiple_yields()
print("first call")
next(gen)
print("second call")
next(gen)
print("third call")
next(gen)

# yield from

# generator expressions
