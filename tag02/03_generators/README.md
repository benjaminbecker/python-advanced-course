# Aufgabe

Implementieren Sie eine Funktion, die die folgende Folge ausgibt:

```python
alternator(100)
0, 100, 1, 99, 2, 98, 3, 97 ... 49, 50
```

Verwenden Sie dabei `yield`-Statements.
