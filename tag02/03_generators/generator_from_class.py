class Range:
    def __init__(self, maximum):
        self.maximum = maximum

    def __iter__(self):
        self.value = 0
        return self

    def __next__(self):
        if self.value < self.maximum:
            yield_value = self.value
            self.value += 1
            return yield_value
        else:
            raise StopIteration


class ListWrapper:
    def __init__(self, maximum):
        self.values = list(range(maximum))

    def __iter__(self):
        return iter(self.values)


if __name__ == "__main__":
    r = Range(10)
    for n in r:
        print(n)

    for n in ListWrapper(10):
        print(n)
