# Simulation of Job-Shop Production System

## Implementierungsidee:

```plantuml
@startuml
package job_shop {
    class Product {
        str name
        uint id_current_process_step
        select_and_transfer_to_SP()
    }
    class ProcessStep {
        processingTimeForSP(ServiceProvider)
        setupTimeForSP(ServiceProvider)
        finish()
        bool finished
    }
    class ServiceProvider {
        enqueue(ProcessStep)
        setupFromQueue()
        process()
    }
    class ProcessQueue {
        append(ProcessStep)
        ProcessStep pop()
    }
    abstract DecisionStrategy {
        ServiceProvider chooseSP(ProcessStep)
    }

    class DefaultDecisionStrategy

    Product --> "1..n" ProcessStep
    ProcessStep --> "1..n" ServiceProvider
    ServiceProvider --> ProcessQueue
    ProcessQueue --> "0..n" ProcessStep
    Product --> DecisionStrategy
    DefaultDecisionStrategy --|> DecisionStrategy
}

package simulation {
    class ProductSimulator
    class ServiceProviderSimulator
}

package reinforcement_learning {
    class RLDecisionStrategy
}

ProductSimulator --> Product
ServiceProviderSimulator --> ServiceProvider
RLDecisionStrategy --|> DecisionStrategy
@enduml
```
