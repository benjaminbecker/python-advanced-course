# Dekoratoren

Ein Dekorator ändert das Verhalten einer Funktion, indem man ihn vor die Definition der Funktion schreibt.

gute Einführung: [realpython.com](https://realpython.com/primer-on-python-decorators/)

# Aufgabe

1. Erweitern Sie den logged-Dekorator so, dass er die Gesamtzahl der Funktionsaufrufe zählt.
2. Implementieren Sie eine Klasse für ein Werkzeug. Die Klasse soll eine Methode `use(duration, comment)` haben. Bei jeder Nutzung soll ein Eintrag in ein Logbuch erfolgen. Über eine dynamisch generierte Property `total_use` soll die Gesamtnutzungsdauer berechnet werden.

```python
t = Tool()
t.use(10, "Job erledigt")
t.use(20, "Maschine gereinigt")
t.use(10, "vorzeitig abgebrochen")
assert t.total_use == 40
```

```plantuml
@startuml
class Tool {
    + int total_use
    + use(int)
}
class Log {
    + add(int duration, str comment)
    - list __items
    + list get_durations()
}
Tool --> Log
@enduml
```
