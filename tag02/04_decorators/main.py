def logged(f):
    """
    """
    def wrapped(*args):
        """
        """
        print(f"called {f.__name__}({args[0], args[1]})")
        return f(*args)

    return wrapped


def add(a, b):
    return a + b


def mult(a, b):
    result = 0
    for _ in range(b):
        result = logged(add)(result, a)
    return result


if __name__ == "__main__":
    print(logged(add)(12, 13))
    print(logged(mult)(11, 10))
    print(logged(add)(1, 2))
