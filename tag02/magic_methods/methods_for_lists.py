class ListWrapper:
    def __init__(self, a_list):
        self.values = a_list

    def __getitem__(self, index):
        return self.values[index]

    def __setitem__(self, index, value):
        self.values[index] = value

    def __len__(self):
        return len(self.values)

    def some_other_method(self):
        pass


if __name__ == "__main__":
    dw = ListWrapper([1, 2, 3, 4])
    print(f"dw[0:3] = {dw[0:3]}")

    dw[3] = 40
    print(f"dw.values[3] = {dw.values[3]}")
    print(f"dw[3] = {dw[3]}")

    print(f"len(dw) = {len(dw)}")
