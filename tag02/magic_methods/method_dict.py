class Person:
    def __init__(self, firstname, secondname):
        self.firstname = firstname
        self.secondname = secondname

    def do_something(self):
        pass


if __name__ == "__main__":
    p = Person("Hansi", "Müller")
    print(p.__dict__)
