# Ist-Zustand

```plantuml
@startuml
    class Machine{
        str name
        str SN
        save()
    }
    class Database{
        write()
        read()
    }
    class Main{
    }

    Machine ..> Database : depends on
    Main ..> Machine : depends on
@enduml
```

## Problem

Die Architektur ist ungünstig:

- Modul `core` ist abhängig vom Modul `database`, Kopplung erschwert Änderungen
- Die Datenbank soll ein Implementierungsdetail sein, was erst in der Anwendung (Modul `main`) festgelegt werden soll
- Die Applikation lässt sich schwer testen, da der Datenbankzugriff schwer zu ersetzen ist.

![Clean Architecture](https://blog.cleancoder.com/uncle-bob/images/2012-08-13-the-clean-architecture/CleanArchitecture.jpg)
Abbildung aus Robert C. Martin: "Clean Architecture"
[Clean Architecture](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

# Soll-Zustand

```plantuml
@startuml
    class Machine{
        str name
        str SN
        save()
    }
    class DatabaseInterface{
        write()
        read()
    }
    class Database{
        write()
        read()
    }
    class Main{
    }


    Machine --> DatabaseInterface
    Database --|> DatabaseInterface
    Main ..> Machine : depends on
    Main ..> Database : depends on
@enduml
```

## Aufgabe:

Implementieren Sie den Soll-Zustand!

### Hinweis

`abc.ABC` aus der [Python Standard Library](https://docs.python.org/3/library/abc.html) ermöglicht das Erstellen abstrakter Klassen.
