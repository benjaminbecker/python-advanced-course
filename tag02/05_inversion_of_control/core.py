from database import db


class Machine:
    def __init__(self, name, sn):
        self.name = name
        self.sn = sn

    def save(self):
        db.write(f"{self.name}, {self.sn}")
