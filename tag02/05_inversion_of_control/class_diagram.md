# Ist-Zustand

```plantuml
@startuml
    class Machine{
        str name
        str SN
        save()
    }
    class Database{
        write()
        read()
    }
    class Main{
    }

    Machine ..> Database : depends on
    Main ..> Machine : depends on
@enduml
```

# Soll-Zustand

```plantuml
@startuml
    class Machine{
        str name
        str SN
        save()
    }
    class DatabaseInterface{
        write()
        read()
    }
    class Database{
        write()
        read()
    }
    class Main{
    }


    Machine --> DatabaseInterface
    Database --|> DatabaseInterface
    Main ..> Machine : depends on
    Main ..> Database : depends on
@enduml
```
