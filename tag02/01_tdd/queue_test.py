from unittest import TestCase
from .queue import Queue


class TestQueue(TestCase):
    def test_is_empty(self):
        q = Queue()
        self.assertTrue(q.isempty)
