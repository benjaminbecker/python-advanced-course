# Entwicklung eines Tools zur Reservierung von Werkzeugen

## Anwendungsfälle

Es soll ein Kommandozeilenprogramm entwickelt werden, über welches Reservierungen von Werkzeugen vorgenommen werden können. Ein Werkzeug ist im Zeitraum von 8 bis 19 Uhr reservierbar. Reservierungen sind nur für volle Stunden möglich. Versucht ein Nutzer ein bereits reserviertes Werkzeug zu reservieren, so erhält er eine Fehlermeldung. Es soll außerdem möglich sein, Reservierungen rückgängig zu machen und den aktuellen Plan der Reservierungen einzusehen.

```plantuml
@startuml
:Nutzer: as user
user --> (Liste der Reservierungen eines Werkzeugs ansehen)
user --> (Ein Werkzeug reservieren)
user --> (Reservierung aufheben)
@enduml
```

## Klassendiagramm

```plantuml
@startuml
class Tool {
    str name
    reserve(Person, start, end)
}

class Schedule {
    print()
    reserve(Person, start, end)
}
class User
Tool --> Schedule
Schedule --> "0..n" User
@enduml
```

## API

Das Tool soll folgendermaßen benutzbar sein:

```
>> hammer = Tool("hammer")
>> saw = Tool("saw")

>> peter = User("Peter")
>> manuela = User("Manuela")

>> hammer.print_schedule()
08  frei
09  frei
10  frei
11  frei
12  frei
13  frei
14  frei
15  frei
16  frei
17  frei
18  frei

>> hammer.reserve(peter, 9, 11)
>> hammer.print_schedule()
08  frei
09  Peter
10  Peter
11  frei
12  frei
13  frei
14  frei
15  frei
16  frei
17  frei
18  frei

>> hammer.reserve(manuela, 10, 11)
Reservierung nicht möglich.

>> hammer.cancel(peter, 10, 11)
>> hammer.print_schedule()
08  frei
09  Peter
10  frei
11  frei
12  frei
13  frei
14  frei
15  frei
16  frei
17  frei
18  frei
```
