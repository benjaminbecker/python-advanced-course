# Test Driven Development

Beim Test-Driven Development (TDD) wird vor dem Schreiben eines Teils des Quellcodes jeweils ein Unit Test geschrieben.

Dabei werden die folgenden drei Schritte zyklisch ausgeführt:

1. Schreibe einen Test, der fehlschlägt.
2. Schreibe genau soviel Code, dass der Test nicht mehr fehlschlägt.
3. Refactoring von Quellcode und Test-Code.

Diese Vorgehensweise führt zu einer vollständigen Abdeckung des Quellcodes durch Unit Tests. Dies vereinfacht Refactorings des Codes, dokumentiert den Code und kann zu einer besseren Software-Architektur führen.

# Einführung

[https://testdriven.io/blog/modern-tdd/](https://testdriven.io/blog/modern-tdd/)

[Python Doku Unit-Tests](https://docs.python.org/3/library/unittest.html)

# Aufgabe

Entwickeln Sie das Tool zur Werkzeugreservierung (siehe README im Unterordner `werkzeugreservierung`). Entwickeln Sie mit TDD.
