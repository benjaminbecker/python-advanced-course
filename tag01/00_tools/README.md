# Tools zur Arbeit mit Python

In der Zusammenarbeit im Team ist es essenziell, dass jeder Entwickler auf einfachem Weg eine Entwicklungsumgebung aufbauen kann, die zu reproduzierbaren Ergebnissen führt. Dazu sind die folgenden Tools hilfreich:

## Reproduzierbare Entwicklungsumgebung

### Virtuelle Umgebungen:

Motivation: [siehe Python Doku](https://docs.python.org/3/tutorial/venv.html)

Hilfreiches Tool: [pipenv](https://github.com/pypa/pipenv)

### Docker

Docker-Container können sowohl für das Deployment als auch zum Entwickeln benutzt werden. Das Dockerfile sollte dann mit dem Programmcode in die Versionsverwaltung mit aufgenommen werden.

### Virtuelle Maschinen

Virtuelle Maschinen sind eine weitere Möglichkeit für reproduzierbare Umgebungen, lassen sich aber schwieriger durch Code beschreiben.

#### Workflow

Der folgende Workflow funktioniert unter Linux. Für Windows sind teilweise Anpassungen nötig.

Nutzer 1 legt neues Projekt an:

```shell
python3 -m venv .venv
. ./.venv/bin/activate
pip install numpy
pip freeze > requirements.txt
git add .
git commit -m"Numpy hinzugefügt"
git push
```

Nutzer 2 will weiterentwickeln:

```shell
git pull
python3 -m venv .venv # falls noch kein virtual environment existiert
. ./.venv/bin/activate
pip install -r requirements.txt
pip install simpy
pip freeze > requirements.txt
git add .
git commit -m"Simpy hinzugefügt"
git push
```

### Plugins für IDE

Verwenden alle Entwickler denselben Editor, so ist es sinnvoll, die Konfigurationsdatei für Python-spezifische Plugins in die Versionsverwaltung zu integrieren.

## REPL

Neben der Standard-Python-Kommandozeile (Kommando `python`) existieren komfortablere Alternativen. Beispiel: [bpython](https://bpython-interpreter.org/)

# Aufgabe

1. Erzeugen Sie ein Virtual Environment (lokal oder mit pipenv in gitpod).
2. Installieren Sie bpython und berechnen Sie das Verhältnis zwischen Monddurchmesser und Erddurchmesser.
3. Wie ist das Verhältnis der Volumina?
