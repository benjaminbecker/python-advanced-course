# Private Methoden und Properties

Eine Klasse sollte für den Benutzer eine Schnittstelle haben, die genau das bietet, was die Klasse tun soll. Implementierungsdetails sollen für den Nutzer unerheblich sein.

Vorteile:

- Fokusierung beim Entwickeln auf Teilprobleme
- Änderung der Implementierung ohne Abhängigkeiten berücksichtigen zu müssen
- Dokumentation durch Code

## Implementierung in Python

1. per Konvention:

```python
class Foo:
    def _dont_use_this(self):
        self._private_property = 12
```

2. etwas strikter

```python
class Foo:
    def __dont_use_this(self):
        self.__private_property = 12
```

# Aufgabe

Verwenden sie die Function `dir` um den Effekt der beiden Varianten zu untersuchen.
