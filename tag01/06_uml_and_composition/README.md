# UML

Übersichtsartikel: [Wikipedia](https://de.wikipedia.org/wiki/Unified_Modeling_Language)

## Beispiele für Klassendiagramme

### Vererbung

Vererbung wird verwendet bei `Kind` ist ein `Elternteil`

```plantuml
@startuml
Elternteil <|-- Kind
@enduml
```

### Komposition

Komposition wird verwendet für Beziehung `A` hat `B`. Z.B. `Person` hat `Auto`

Diese Beziehung kann in eine oder auch in beide Richtungen sein. Falls ausreichend ist einseitig vorzuziehen.

#### Einseitige Beziehung

```plantuml
@startuml
class Person
class Auto
Person --> Auto
@enduml
```

#### Beidseitige Beziehung

```plantuml
@startuml
class Maschinenpark
class Maschine
Maschinenpark -- Maschine
@enduml
```

#### Kardinalität

Die mögliche Anzahl an Objekten kann über die Kardinalität angegeben werden.

```plantuml
@startuml
class Person
class Auto
class Personalausweis
Person "0..n" --> "0..n" Auto
Person "1" --> "1" Personalausweis
class Maschinenpark
class Maschine
Maschinenpark "1" -- "0..n" Maschine
@enduml
```

# Aufgabe

Implementieren Sie folgende Paare:

1. `Maschinenpark`, `Maschine`
2. `Person`, `Auto`

Warum sind einseitige Beziehungen vorzuziehen?
