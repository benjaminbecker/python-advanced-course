class Car:
    def __init__(self, model, power, owner_first_name, owner_second_name, owner_city):
        self.model = model
        self.power = power
        self.owner_first_name = owner_first_name
        self.owner_second_name = owner_second_name
        self.owner_city = owner_city

    def print_car_info(self):
        print(
            f"Dieses Auto ist ein {self.model} mit {self.power} PS. Es gehört {self.owner_first_name} {self.owner_second_name} aus {self.owner_city}")


if __name__ == "__main__":
    mercedes = Car("Mercedes", 243, "Benjamin", "Becker", "Hamburg")
    mercedes.print_car_info()
