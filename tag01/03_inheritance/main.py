class Machine:
    def __init__(self, SN):
        self.SN = SN

    def print_serial_number(self):
        print(f"SN: {self.SN}")


class DrillingMachine(Machine):
    def __init__(self, SN, max_speed):
        super().__init__(SN)
        self.max_speed = max_speed

    def print_serial_number(self):
        print("Drilling machine")
        super().print_serial_number()

    def drill(self):
        sound = 'b' + ''.join(['r' for _ in range(self.max_speed)])
        print(sound)


if __name__ == "__main__":
    dm = DrillingMachine(123, 100)
    dm.print_serial_number()
    dm.drill()
