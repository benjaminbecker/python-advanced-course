```plantuml
@startuml
class Machine{
    str SN
    print_serial_number()
}
class DrillingMachine{
    int max_speed
    drill()
}
package "Übung" {
    class MillingMachine{
        int max_width
        int max_height
        int max_depth
        set materials
        bool can_handle(width, height, depth, material)
    }
}
Machine <|-- DrillingMachine : erbt von
Machine <|-- MillingMachine: erbt von
@enduml
```

# Aufgaben

1. Implementieren Sie die Klasse `MillingMachine`. Testfälle:

```python
m = MillingMachine(12345, max_width=10, max_height=10, max_depth=10, materials={"Aluminum", "Wood"})
assert m.can_handle(width=1, height=1, depth=1, material="Wood") == True
assert m.can_handle(width=1, height=1, depth=1, material="Steel") == False
assert m.can_handle(width=11, height=1, depth=1, material="Aluminum") == False
```

2. Finden Sie eine geeignete Abstraktion für ein Bauteil (`width, height, depth, material`), sodass folgende Syntax möglich wird:

```python
part = ...
assert m.can_handle(part) == True
```
