from unittest import TestCase
from .module1 import add

class TestAdd(TestCase):
    def test_it_adds(self):
        self.assertEqual(add(1, 1), 2)