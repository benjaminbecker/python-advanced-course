from folder_containing_module1.module1 import add
import folder_containing_module1.module1 as m1
from folder_containing_module1 import ANOTHER_CONSTANT

if __name__ == "__main__":
    print(f"1 + 1 = {add(1, 1)}")
    print(f"A_CONSTANT = {m1.A_CONSTANT}")
    print(f"ANOTHER_CONSTANT = {ANOTHER_CONSTANT}")
