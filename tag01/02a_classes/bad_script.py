if __name__ == "__main__":
    machine1_name = "Maschine 1"
    machine2_name = "Maschine 2"

    machine1_capacity = 12
    machine2_capacity = 15

    def print_machine_description(name, capacity):
        print(f"{name}: Kapazität: {capacity}")

    print_machine_description(machine1_name, machine1_capacity)
    print_machine_description(machine2_name, machine2_capacity)
