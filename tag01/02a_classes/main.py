class Greeter:
    def greet(self):
        print(f"Hallo, Benjamin.")


if __name__ == "__main__":
    greeter = Greeter()
    greeter.greet()
