# Klassen

[Python Doku](https://docs.python.org/3/tutorial/classes.html)

## Aufgaben

1. Erweitern Sie die Klasse `Greeter` so, dass Nachname als Property mit aufgenommen wird.
2. Bauen Sie `bad_script.py` in ein objektorientiertes Programm um.
