from dataclasses import dataclass


def plumages(birds):
    return {b.name: plumage(b) for b in birds}


def speeds(birds):
    return {b.name: airSpeedVelocity(b) for b in birds}


def plumage(bird):
    if bird._type == 'EuropeanSwallow':
        return "average"
    elif bird._type == 'AfricanSwallow':
        return "tired" if bird.numberOfCoconuts > 2 else "average"
    elif bird._type == 'NorwegianBlueParrot':
        return "scorched" if bird.voltage > 100 else "beautiful"
    return "unknown"


def airSpeedVelocity(bird):
    if bird._type == 'EuropeanSwallow':
        return 35
    elif bird._type == 'AfricanSwallow':
        return 40 - 2 * bird.numberOfCoconuts
    elif bird._type == 'NorwegianBlueParrot':
        return 0 if bird.is_nailed else 10 + bird.voltage / 10
    return None


@dataclass
class Bird:
    name: str
    _type: str
    numberOfCoconuts: int
    voltage: int
    is_nailed: bool


if __name__ == "__main__":
    birds = [
        Bird(name="Tweety", _type='EuropeanSwallow', numberOfCoconuts=1,
             voltage=10, is_nailed=False),
        Bird(name="Piepsi", _type='AfricanSwallow', numberOfCoconuts=4,
             voltage=10, is_nailed=False),
        Bird(name="Piepmatz", _type='NorwegianBlueParrot',
             numberOfCoconuts=1, voltage=10, is_nailed=True),
    ]
    print("Plumages: ", plumages(birds))
    print("Speeds: ", speeds(birds))
