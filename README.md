# Python Advanced Course

## Online-Editor gitpod

Hier können Sie mit dem Quelltext dieses Kurses arbeiten:

[Link](https://gitpod.io/#https://gitlab.com/benjaminbecker/python-advanced-course)

## Lösungen

Die Lösungen zu den Übungsaufgaben befinden sich im Branch `solutions`

## Tag 1

|       |                                            |
| ----- | ------------------------------------------ |
| 9:00  | Begrüßung, Kennenlernen                    |
|       | Einrichtung Entwicklungsumgebung           |
|       | Tools: bpython, virtualenv, pipenv         |
| 10:00 | Module, Übung                              |
|       | Scope, Data Model, Übung mutable/immutable |
|       | Zusammenfassung                            |
| 12:00 | <b>Mittagspause</b>                        |
| 13:00 | Klassen/Objekte, Übung                     |
|       | Vererbung, Übung                           |
| 14:00 | private Methoden und Properties            |
| 15:00 | Refactoring, Übung                         |
| 16:00 | UML Basics                                 |
|       | Komposition, Übung                         |

## Tag 2

|       |                                    |
| ----- | ---------------------------------- |
| 09:00 | Begrüßung                          |
| 9:15  | TDD                                |
|       | Übung Werkzeugreservierung         |
| 10:00 | Pause                              |
| 10:15 | Exceptions, Übung                  |
| 11:00 | Grundlagen Generatoren             |
|       | Generator Expressions              |
|       | Generatoren in Klassen             |
|       | Übung Alternierende Folge          |
| 12:00 | <b>Mittagspause</b>                |
| 13:00 | Grundlagen Dekoratoren             |
|       | Property Decorator, Übung          |
|       | Static Method Decorator, Übung     |
| 14:00 | Einführung Anwendungsbeispiel      |
|       | Inversion of Control               |
|       | OOD Problemdomäne                  |
|       | Implementierung Anwendungsbeispiel |

## Literaturempfehlungen

### Python

#### Cosmic Python Book

[https://github.com/cosmicpython/book](https://github.com/cosmicpython/book)

Softwarearchitektur für Python-Web-Applikationen

#### Luciano Ramalho: Fluent Python

behandelt eher fortgeschrittene Themen der Programmierung in Python

#### Dan L. Bader: Python Tricks

lose Sammlung hilfreicher Themen

### Software-Entwicklung allgemein

#### Robert C. Martin: Clean Code

Anleitung zum Schreiben von "sauberem" Code

#### Robert C. Martin: Clean Architecture

eher fortgeschritten, Strukturierung von Software

#### Kent Beck: Test-Driven Development

sehr gute und gut zu lesende Einführung in die Erstellung von Software basierend auf Tests

#### Eric Evans: Domain-driven design

eher fortgeschritten, Modellierung von Systemen durch objektorientierte Programmierung

[Vortrag des Autors](https://www.youtube.com/watch?v=pMuiVlnGqjk)
